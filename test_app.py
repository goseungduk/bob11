import unittest
import app

class TestBOB(unittest.TestCase):
    def test_add(self):
        instance = app.BOB()
        result = instance.add(2,3)
        self.assertEqual(result, '5')
    def test_sub(self):
        instance = app.BOB()
        result = instance.sub(100,99)
        self.assertEqual(result, '1')

if __name__ == "__main__":
    unittest.main()