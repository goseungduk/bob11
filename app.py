from flask import Flask, request
app = Flask(__name__)

class BOB:
    def home(self):
        return 'hello bob from user123'
    def add(self,a,b):
        return str(int(a) + int(b))
    def sub(self,a,b):
        return str(int(a) - int(b))

@app.route('/', methods=['GET'])
def home():
    bob_obj = BOB()
    return bob_obj.home()

@app.route('/add', methods=['GET'])
def add():
    bob_obj = BOB()
    return bob_obj.add(request.args.get('a'),request.args.get('b'))
   
@app.route('/sub', methods=['GET'])
def sub():
    bob_obj = BOB()
    return bob_obj.sub(request.args.get('a'), request.args.get('b'))

if __name__ == '__main__':  
   app.run('0.0.0.0', port=8123, debug=True)

